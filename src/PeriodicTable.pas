(* * * * * * * * * * * * * === Periodic Table === * * * * * * * * * * * *
 *      Version: 1.0.0                                                  *
 * Release date: December 31st, 2012                                    *
 *      License: GNU GPL 3.0  (included with the source)                *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt  *
 *                                                                      *
 *  Description: The main program.                                      *
 * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
program PeriodicTable_100EN;
uses crt, classes, sysutils, {$IFDEF WIN32}windows,{$ENDIF} UAuxiliar;

const // Desenho
      {$IFDEF WIN32}
      C_UP    = #196;
      C_DOWN  = C_UP;
      C_LEFT  = #179;
      C_RIGHT = C_LEFT;
      C_CORNER_UPLEFT      = #218;
      C_CORNER_UPRIGHT     = #191;
      C_CORNER_DOWNLEFT    = #192;
      C_CORNER_DOWNRIGHT   = #217;
      C_CROSS_LATERALLEFT  = #180;
      C_CROSS_LATERALRIGHT = #195;
      C_CROSS_TOPDOWN = #194;
      C_CROSS_TOPUP   = #193;
      C_CROSS_ALL     = #197;
      {$ELSE}
      C_UP    = '─';
      C_DOWN  = C_UP;
      C_LEFT  = '│';
      C_RIGHT = C_LEFT;
      C_CORNER_UPLEFT      = '┌';
      C_CORNER_UPRIGHT     = '┐';
      C_CORNER_DOWNLEFT    = '└';
      C_CORNER_DOWNRIGHT   = '┘';
      C_CROSS_LATERALLEFT  = '┤';
      C_CROSS_LATERALRIGHT = '├';
      C_CROSS_TOPDOWN = '┬';
      C_CROSS_TOPUP   = '┴';
      C_CROSS_ALL     = '┼';
      {$ENDIF}
      
      // Definições gerais - não mexer nestes valores!
      Cell_Height = 2;
      Cell_Width  = 4;
      Table_X = 18;
      Table_Y = 10;
      Table_Height = Cell_Height * Table_Y;
      Table_Width  = Cell_Width * Table_X;
      BkColor = 0;
      TxtColorNumbers = 8;
      TxtColorTable = 15;
      
      InitPos : record x,y:word end = (x:1; y:1);
      
      // Database
      ATOMS_DB : string = 'DB.periodic';
      IND_PERIOD    = 0;
      IND_GROUP     = 1;
      IND_Z         = 2;
      IND_SYMBOL    = 3;
      IND_YEAR      = 4;
      IND_ELECTRON  = 5;
      IND_IONENERGY = 6;
      IND_NAME      = 7;
      
      MAX_ELEM = 118;

type TElemType = (ETNumber, ETSymbol);

var table : TStringList;  // Guarda os dados da Database ao arrancar.
    Table_Only_Group  : TStringList;
    Table_Only_Period : TStringList;
    Table_Only_XY     : TStringList;
    Table_Only_Symbol : TStringList;


procedure RaiseErrorEvent(ErrType, ErrText : string);
(* Mostra erro que tenha ocorrido durante a execução do programa, e termina-o. *)
const TOP = 5;
      LAT = 10;
      TITLE  = 'PERIODIC TABLE 1.0.0 EN';
      AUTHOR = 'Igor Nunes Software, 2013';

var x, y : word;

begin
   TextBackground(0);
   ClrScr;
   
   TextBackground(4);
   for y := TOP to 25-TOP do begin
      GotoXY(LAT, y);
      for x := LAT to 80-LAT do write(' ');
   end;
   
   TextColor(12);
   GotoXY(40 - length(TITLE) div 2, TOP);
   write(TITLE);
   TextColor(16);
   GotoXY(40 - length(AUTHOR) div 2, TOP+1);
   write(AUTHOR);
   
   TextColor(15);
   GotoXY(LAT+1, TOP+3);
   write('The application cannot proceed.');
   GotoXY(LAT+1, TOP+4);
   write('An unexpected and fatal error has occurred.');
   GotoXY(LAT+1, TOP+6);
   write('TECNICAL DESCRIPTION:');
   GotoXY(LAT+1, TOP+7);
   write('  Class: '); TextColor(14); write(ErrType);
   TextColor(15);
   GotoXY(LAT+1, TOP+8);
   write('Message: '); TextColor(14); write(ErrText);
   
   TextColor(15);
   GotoXY(LAT+1, TOP+10);
   write('Report this error to the producer.');
   
   GotoXY(LAT+1, 24-TOP);
   TextColor(7);
   Pause('[ENTER] to close...');
   halt;
end;

procedure RaiseErrorEvent(excpt : Exception); overload;
(* Ocorreu um erro inesperado. *)
begin
   RaiseErrorEvent(excpt.ClassName, excpt.Message);
end;


function FileExists(name : string): boolean;
(* Verifica se um ficheiro existe. *)
var fich : text;
begin
   assign(fich, name);
   {$I-};
   Reset(fich);
   Close(fich);
   {$I+};
   FileExists := (IoResult=0);
end;


function ElementColor(X, Y : word) : word;
const 
      REPRESENTATIVE = 10;
      TRANSITION     = 11;
      NOBLE_GAS      = 9;
      LANTHANIDES    = 13;
      ACTINIDES      = LANTHANIDES;

begin
   if (x = 17) and (y in [0..6]) then ElementColor := NOBLE_GAS
   else if (x in [0,1]) or ((x in [12..17]) and (y in [0..6])) then ElementColor := REPRESENTATIVE
   else if (x in [2..11]) and (y in [3..6]) then ElementColor := TRANSITION
   else ElementColor := LANTHANIDES;
end;


function ElementColor(elem : word) : word; overload;
const 
      REPRESENTATIVE = 10;
      TRANSITION     = 11;
      NOBLE_GAS      = 9;
      LANTHANIDES    = 13;
      ACTINIDES      = LANTHANIDES;

begin
   case elem of
      1,3..9,11..17,19,20,31..35,37,38,49..53,55,56,81..85,87,88,113..117 : ElementColor := REPRESENTATIVE;
      2,10,18,36,54,86,118 : ElementColor := NOBLE_GAS;
      57..71 : ElementColor := LANTHANIDES;
      89..103 : ElementColor := ACTINIDES;
      21..30,39..48,72..80,104..112 : ElementColor := TRANSITION;
   end;
end;


procedure DrawTable(PosX, PosY : word);
(* Desenha a tabela periódica, mas sem os elementos. *)

   function Spaces(const n : integer) : string;
   (* Devolve 'n' espaços *)
   var i : integer;
      s : string = '';
   begin
      for i := 1 to n do s := s + ' ';
      Spaces := s;
   end;

var x, y : integer;
begin
   TRY
   TextBackground(BkColor);
   if not((PosX + Table_Width > 80) or (PosY + Table_Height > 25)) then begin
      GotoXY(PosX + 4, PosY);
      TextColor(TxtColorNumbers);
      for x := 1 to Table_X do write(x:2,Spaces(Cell_Width-2));  // escreve nº grupos
      
      // desenha tabela
      for y := 0 to Table_Height do begin
         GotoXY(PosX, PosY + y + 1);
         
         // escreve nº período
         TextColor(TxtColorNumbers);
         if ((y-1) mod Cell_Height = 0) and not (y = 7*Cell_Height + 1) then begin
            if (y < 7*Cell_Height) then write(y div Cell_Height + 1 : 2, ' ')
            else write(y div Cell_Height - 2: 2, ' ')
         end else write('   ');
         
         TextColor(TxtColorTable);
         // desenha linha actual
         if not(y in [7*Cell_Height+1 .. 8*Cell_Height-1]) then
            for x := 0 to Table_Width do begin
               // Casos especiais
               if ((x = Cell_Width) and (y = 0)) or ((x = 2*Cell_Width) and (y = Cell_Height)) then write(C_CORNER_UPRIGHT)
               else if ((x = 17 * Cell_Width) and (y = 0))
                     or ((x = 12*Cell_Width) and (y = Cell_Height))
                     or ((x = 3*Cell_Width) and (y = 8*Cell_Height)) then write(C_CORNER_UPLEFT)
               else if ((y in [0, Cell_Height-1]) and (x in [Cell_Width+1 .. 17*Cell_Width-1]))
                     or ((y in [Cell_Height .. 3*Cell_Height-1]) and (x in [2*Cell_Width+1 .. 12*Cell_Width-1])) then write(' ')
               else if ((y = 3*Cell_Height) and (x mod Cell_Width = 0) and (x in [2*Cell_Width+1 .. 12*Cell_Width-1]))
                     or ((y = Cell_Height) and (x mod Cell_Width = 0) and (x in [13*Cell_Width .. 17*Cell_Width-1])) then write(C_CROSS_TOPDOWN)
               else if (y in [2*Cell_Height, 6*Cell_Height]) and (x = 2*Cell_Width) then write(C_CROSS_LATERALLEFT)
               else if ((y = 2*Cell_Height) and (x = 12*Cell_Width))
                     or ((y = 9*Cell_Height) and (x = 3*Cell_Width))
                     or ((y = 6*Cell_Height) and (x = 3*Cell_Width)) then write(C_CROSS_LATERALRIGHT)
               else if (y in [8*Cell_Height .. Table_Height]) and (x in [0 .. 3*Cell_Width-1]) then write(' ')
               else if (y in [7*Cell_Height, Table_Height]) and (x = 3*Cell_Width) then write(C_CORNER_DOWNLEFT)
               else if (y in [6*Cell_Height .. 7*Cell_Height]) and (x in [2*Cell_Width+1 .. 3*Cell_Width-1]) then write(' ')
               else if (y = 7*Cell_Height) and (x = 2*Cell_Width) then write(C_CORNER_DOWNRIGHT)
               // Caso geral
               else if (y mod Cell_Height = 0) then begin
                  if (x mod Cell_Width = 0) then begin
                     if (x = 0) then begin
                        if (y in [0, 16]) then write(C_CORNER_UPLEFT)
                        else if (y in [14, Table_Height]) then write(C_CORNER_DOWNLEFT)
                        else write(C_CROSS_LATERALRIGHT);
                     end else if (x = Table_Width) then begin
                        if (y in [0, 16]) then write(C_CORNER_UPRIGHT)
                        else if (y in [14, Table_Height]) then write(C_CORNER_DOWNRIGHT)
                        else write(C_CROSS_LATERALLEFT);
                     end else if (y in [0, 16]) then write(C_CROSS_TOPDOWN)
                     else if (y in [14, Table_Height]) then write(C_CROSS_TOPUP)
                     else write(C_CROSS_ALL);
                  end else write(C_UP);
               end else if (x mod Cell_Width = 0) then write(C_LEFT)
                      else write(' ');
            end;
      end;
      
      TextColor(2);
      GotoXY(PosX + 4*Cell_Width - 1, PosY + Cell_Height + 1);
      write('Igor Nunes Software, 2013');
      TextColor(3);
      GotoXY(PosX + 4*Cell_Width - 1, PosY + Cell_Height + 2);
      write('PERIODIC TABLE, 1.0.0-beta (EN)');
      
   end else begin
      RaiseErrorEvent('Graphics', 'Table cannot be drawn under the given parameters.');
   end;
   
   EXCEPT
      on ex : Exception do RaiseErrorEvent(ex);
   END;
end;


procedure DrawElements(PosX, PosY : word; ElemType : TElemType);
(* Escreve os elementos na tabela. *)
var s : string;
    elem : TStringList;
    x, y : word;

begin
   TRY
   TextBackground(0);
   
   for s in table do begin
      elem := TStringList.Create;
      Split(s, '|', elem);
      x := str2int(elem[IND_GROUP]) - 1;
      y := str2int(elem[IND_PERIOD]) - 1;
      
      GotoXY(PosX + 4 + x*Cell_Width, PosY + 2 + y*Cell_Height);
      
      // selecção de cores
      TextColor(ElementColor(x, y));
      
      case ElemType of
         ETNumber : write(elem[IND_Z]:3);
         ETSymbol : write(elem[IND_SYMBOL]:3);
      end;
      elem.Destroy;
   end;
   
   EXCEPT
      on ex : Exception do RaiseErrorEvent(ex);
   END;
end;


procedure DrawButton(const PosX, PosY, Width, Height : word; const s : string; const TxtColor, BkColor : word; const frame : boolean);
(* Desenha um botão, com ou sem contorno (dado por 'frame') *)
var x, y : word;
begin
   TRY
   TextBackground(BkColor);
   TextColor(TxtColor);
   if frame then
   for y := 1 to Height do begin
      GotoXY(PosX, PosY + y - 1);
      for x := 1 to Width do begin
         if (x = 1) then begin
            if (y = 1) then write(C_CORNER_UPLEFT)
            else if (y = Height) then write(C_CORNER_DOWNLEFT)
            else write(C_LEFT);
         end else if (x = Width) then begin
            if (y = 1) then write(C_CORNER_UPRIGHT)
            else if (y = Height) then write(C_CORNER_DOWNRIGHT)
            else write(C_RIGHT);
         end else if (y = 1) then write(C_UP)
         else if (y = Height) then write(C_DOWN)
         else write(' ');
      end;
   end
   else
   for y := 1 to Height do begin
      GotoXY(PosX, PosY + y - 1);
      for x := 1 to Width do write(' ');
   end;
   
   GotoXY(PosX + Width div 2 - length(s) div 2, PosY + Height div 2);
   Write(s);
   
   EXCEPT
      on ex : Exception do RaiseErrorEvent(ex);
   END;
end;


procedure ShowElementInfo(Z : word);
(* Mostra a informação dos elementos, começando pelo elemento com nº atómico 'Z'. *)
var elem : TStringList;
    k : char;

   procedure NextElement(var el : word);
   (* Obtém o próximo elemento *)
   begin
      if el + 1 > MAX_ELEM then el := 1
      else Inc(el);
   end;

   procedure PreviousElement(var el : word);
   (* Obtém o elemento anterior *)
   begin
      if el - 1 < 1 then el := MAX_ELEM
      else Dec(el);
   end;

begin
   elem := TStringList.Create;
   
   REPEAT
   //repeat
   TextBackground(0);
   ClrScr;
   
   TextColor(15);
   GotoXY(5, 1);
   write('ELEMENT INFORMATION');
   
   elem.Clear;
   Split(table[Z-1], '|', elem);
   // BASE: GotoXY(2, 0); TextColor(15); write(''); TextColor(10); write();
   GotoXY(2, 3); TextColor(15); write('Name: '); TextColor(10); write(elem[IND_NAME]);
   GotoXY(2, 4); TextColor(15); write('Chemical symbol: '); TextColor(10); write(elem[IND_SYMBOL]);
   GotoXY(2, 5); TextColor(15); write('Atomic number: '); TextColor(10); write(elem[IND_Z]);
   GotoXY(2, 7); TextColor(15); write('Position in table: Group = '); TextColor(10); write(elem[IND_GROUP]:2); TextColor(15); write(' | Period = '); TextColor(10); write(elem[IND_PERIOD]:2);
   GotoXY(2, 9); TextColor(15); write('Electron configuration: '); TextColor(10); write(elem[IND_ELECTRON]);
   GotoXY(2, 11); TextColor(15); write('Ionization energy (eV): '); TextColor(10); write(elem[IND_IONENERGY]);
   GotoXY(2, 13); TextColor(15); write('Year of discovery: '); TextColor(10); write(elem[IND_YEAR]);
   
   TextColor(7);
   GotoXY(2, 23); write('Left / Right = Navigation through elements  |  ESC = Return to table');
   
   repeat
      k := upcase(readkey);
   until k in [#27, #75, #77];
   
   case k of
      #75 : // esquerda
           PreviousElement(Z);
      #77 : // direita
           NextElement(Z);
   end;
   
   UNTIL k = #27;
   elem.Destroy;
end;


procedure SearchElement(var elem : word);
(* Prompt para pesquisa. *)
const TOP = 23;
      LAT = 2;
var x, y : word;

begin
   repeat
      TextBackground(2);
      for y := TOP to TOP+2 do begin
         GotoXY(LAT, y);
         for x := LAT to 80-LAT+1 do write(' ');
      end;
      
      if not (elem in [1..118]) then begin
         GotoXY(LAT,TOP+2);
         TextColor(14);
         write('The atomic number is between 1 and 118!');
      end;
      
      TextColor(15);
      GotoXY(LAT+1, TOP);
      write('SEARCH FOR ELEMENT');
      GotoXY(LAT+1, TOP+1);
      write('Atomic number? (''0'' to cancel) ');
      TextColor(10);
      readln(elem);
   until elem in [0..118];
   
   (* Procurar elemento e apresentar informações *)
   if not(elem = 0) then ShowElementInfo(elem)
   else begin
      // limpeza à saída
      TextBackground(0);
      for y := TOP to TOP+2 do begin
         GotoXY(LAT, y);
         for x := LAT to 80-LAT+1 do write(' ');
      end;
      TextColor(7);
   end;
end;


procedure NavigateOnTable(InfoToShow : word);
var X : word = 0;
    Y : word = 0;
    Z : word = 1;
    k : char;

const WIDTH = 3;
      HEIGHT_1 = 19;
      HEIGHT_2 = 17;
      BUTTON_SYMBOL_TEXT = 'Chemical symbol';
      BUTTON_NUMBER_TEXT = 'Atomic Number';
      PosX = 5;
      PosY = 23;

   procedure NextElement(var el : word);
   (* Obtém o próximo elemento *)
   begin
      if el + 1 > MAX_ELEM then el := 1
      else Inc(el);
   end;

   procedure PreviousElement(var el : word);
   (* Obtém o elemento anterior *)
   begin
      if el - 1 < 1 then el := MAX_ELEM
      else Dec(el);
   end;

   procedure UpElement(var el : word);
   begin
      case el of
         1 : el := 87;
         2 : el := 118;
         3 : el := 1;
         4 : el := 88;
         5..9 : Inc(el, 108);
         10 : el := 2;
         11..20 : Dec(el, 8);
         21 : el := 39;
         22..30 : Inc(el, 82);
         31..56 : Dec(el, 18);
         57..71 : Inc(el, 47);
         72..118 : Dec(el, 32);
      end;
   end;

   procedure DownElement(var el : word);
   begin
      case el of
         1 : el := 3;
         2..12 : Inc(el, 8);
         13..38 : Inc(el, 18);
         39 : el := 57;
         40..86 : Inc(el, 32);
         87 : el := 4;
         88 : el := 21;
         89..97 : Dec(el, 67);
         98..103 : Dec(el, 93);
         104..118 : Dec(el, 47);
      end;
   end;

   procedure DrawCell(bk : boolean);
   var cx, cy : word;
   begin
      if bk then TextBackGround(1)
      else TextBackground(0);
      
      // preenche célula
      if bk then
         for cy := y to y + Cell_Height - 2 do begin
            GotoXY((x-1)*Cell_Width + 5, cy-y + (y-1)*Cell_Height + 3);
            for cx := x to x + Cell_Width - 2 do begin
               write(' ')
            end;
         end;
      
      GotoXY((x-1)*Cell_Width + 5, (y-1)*Cell_Height + 3);
      TextColor(ElementColor(Z));
      
      if InfoToShow = IND_Z then write(Z:3)
      else if InfoToShow = IND_SYMBOL then write(Table_Only_Symbol[Z-1]:3);
      
      TextBackground(0);
   end;

begin
   REPEAT
   x := str2int(Table_Only_Group[Z-1]);
   y := str2int(Table_Only_Period[Z-1]);
   DrawCell(true);
   
   repeat
      k := upcase(readkey);
   until k in [#75, #77, #72, #80, #13, #27, #9];
   
   DrawCell(false);
   case k of
      #75 : PreviousElement(Z);
      #77 : NextElement(Z);
      #72 : UpElement(Z);
      #80 : DownElement(Z);
      #13 : ShowElementInfo(Z);
      #9  : begin
           if InfoToShow = IND_Z then begin
              InfoToShow := IND_SYMBOL;
              DrawElements(InitPos.x, InitPos.y, ETSymbol);
              
              DrawButton(PosX, PosY, HEIGHT_1, WIDTH, BUTTON_SYMBOL_TEXT, 16, 7, true);
              DrawButton(PosX + HEIGHT_1 + 2, PosY, HEIGHT_2, WIDTH, BUTTON_NUMBER_TEXT, 16, 7, false);
           end else begin
              InfoToShow := IND_Z;
              DrawElements(InitPos.x, InitPos.y, ETNumber);
              
              DrawButton(PosX, PosY, HEIGHT_1, WIDTH, BUTTON_SYMBOL_TEXT, 16, 7, false);
              DrawButton(PosX + HEIGHT_1 + 2, PosY, HEIGHT_2, WIDTH, BUTTON_NUMBER_TEXT, 16, 7, true);
           end;
           end;
   end;
   
   UNTIL k in [#13, #27];
end;


procedure About;
var x, y : word;
begin
   TextBackground(0);
   TextColor(3);
   ClrScr;
   
   for y in [2, 24] do begin
      GotoXY(3, y);
      for x := 3 to 78 do write(C_UP);
   end;
   for x in [2, 79] do begin
      for y := 3 to 23 do begin
         GotoXY(x, y);
         write(C_LEFT);
      end;
   end;
   GotoXY(2, 2); write(C_CORNER_UPLEFT);
   GotoXY(79, 2); write(C_CORNER_UPRIGHT);
   GotoXY(2, 24); write(C_CORNER_DOWNLEFT);
   GotoXY(79, 24); write(C_CORNER_DOWNRIGHT);
   TextColor(11);
   GotoXY(32, 2); write(' PERIODIC TABLE ');
   
   GotoXY(4, 4); TextColor(15); write(' Version: '); TextColor(7); write('1.0.0');
   GotoXY(4, 5); TextColor(15); write('   Stage: '); TextColor(7); write('Beta (prototype)');
   GotoXY(4, 6); TextColor(15); write('Language: '); TextColor(7); write('EN (English)');
   GotoXY(4, 8); TextColor(15); write('An application by: '); TextColor(7); write('Igor Nunes');
   GotoXY(4, 9); TextColor(15); write('     Release date: '); TextColor(7); write('January 1st, 2013');
   
   GotoXY(4, 11); TextColor(15); write('About the program:'); TextColor(7);
   GotoXY(4, 12); write('  Navigate through this dynamic periodic table and learn more about the');
   GotoXY(4, 13); write('chemical elements.');
   
   GotoXY(4, 16); write('This application is freeware. Avoid plagiarism and fakes.');
   
   GotoXY(3, 23); TextColor(8);
   Pause('[ENTER] to return...');
end;


procedure MenuTable(const PosX, PosY : word; var k : char);
(* Alterna a tabela entre nº atómico e símbolo químico, e redirecciona para a prompt de pesquisa ou outro. *)
const BUTTONS = 2;

   procedure NextButton(var Now : word);
   (* Botão seguinte do menu *)
   begin
      if Now + 1 > BUTTONS then Now := 1
      else Inc(Now);
   end;
   
   procedure PreviousButton(var Now : word);
   (* Botão anterior do menu *)
   begin
      if Now - 1 < 1 then Now := BUTTONS
      else Dec(Now);
   end;

const WIDTH    = 3;
      HEIGHT_1 = 19;
      HEIGHT_2 = 17;
      BUTTON_SYMBOL_TEXT = 'Chemical symbol';
      BUTTON_NUMBER_TEXT = 'Atomic Number';

var SelectedButton : word = 1;
    SE : word = 1;

begin
   TRY
   REPEAT
   TextBackground(0); TextColor(8);
   ClrScr;
   GotoXY(45, 23);
   write('Left/Right = Change view           ');
   GotoXY(45, 24);
   write('E = Explore table | ENTER = Search ');
   GotoXY(45, 25);
   write('A = About | ESC = Exit             ');
   
   DrawTable(InitPos.x, InitPos.y);
   DrawButton(PosX, PosY, HEIGHT_1, WIDTH, BUTTON_SYMBOL_TEXT, 16, 7, true);
   DrawButton(PosX + HEIGHT_1 + 2, PosY, HEIGHT_2, WIDTH, BUTTON_NUMBER_TEXT, 16, 7, false);
   DrawElements(InitPos.x, InitPos.y, ETSymbol);
   
   REPEAT
   repeat
      GotoXY(79, 25);
      k := upcase(readkey);
      if k in [#75, #77] then begin
         case k of
            #75 : // esquerda
                 PreviousButton(SelectedButton);
            #77 : // direita
                  NextButton(SelectedButton);
         end;
         
         case SelectedButton of
            1 : begin
               DrawButton(PosX, PosY, HEIGHT_1, WIDTH, BUTTON_SYMBOL_TEXT, 16, 7, true);
               DrawButton(PosX + HEIGHT_1 + 2, PosY, HEIGHT_2, WIDTH, BUTTON_NUMBER_TEXT, 16, 7, false);
               end;
            2 : begin
               DrawButton(PosX, PosY, HEIGHT_1, WIDTH, BUTTON_SYMBOL_TEXT, 16, 7, false);
               DrawButton(PosX + HEIGHT_1 + 2, PosY, HEIGHT_2, WIDTH, BUTTON_NUMBER_TEXT, 16, 7, true);
               end;
         end;
      end;
   until k in [#75, #77] + [#13, #27] + [#69, #65];
   
   if k in [#75, #77] then
      case SelectedButton of
         1 : DrawElements(InitPos.x, InitPos.y, ETSymbol);
         2 : DrawElements(InitPos.x, InitPos.y, ETNumber);
      end
   else if k = #13 then begin
      SearchElement(SE);
      if SE = 0 then begin
         k := #0;
         DrawButton(PosX, PosY, HEIGHT_1, WIDTH, BUTTON_SYMBOL_TEXT, 16, 7, true);
         DrawButton(PosX + HEIGHT_1 + 2, PosY, HEIGHT_2, WIDTH, BUTTON_NUMBER_TEXT, 16, 7, false);
         DrawElements(InitPos.x, InitPos.y, ETSymbol);
      end;
      SelectedButton := 1;
      
      TextBackground(0); TextColor(8);
      GotoXY(45, 23);
      write('Left/Right = Change view           ');
      GotoXY(45, 24);
      write('E = Explore table | ENTER = Search ');
      GotoXY(45, 25);
      write('A = About | ESC = Exit             ');
   end else if k = #69 then begin
      TextColor(8);
      GotoXY(45, 23);
      write('Left/Right = Navigation            ');
      GotoXY(45, 24);
      write('TAB = Change view | ESC = Return   ');
      GotoXY(45, 25);
      write('ENTER = See element information    ');
      
      case SelectedButton of
         1 : NavigateOnTable(IND_SYMBOL);
         2 : NavigateOnTable(IND_Z);
      end;
   end else if k = #65 then About;
   
   UNTIL (k in [#13, #27, #69, #65]);
   UNTIL (k = #27);
   
   EXCEPT
      on ex : Exception do RaiseErrorEvent(ex);
   END;
end;


procedure Verify;
(* Verifica a existência das condições iniciais para correr o programa. *)
begin
   if not FileExists(ATOMS_DB) then RaiseErrorEvent('CRITICAL', 'Database does not exist.');
end;


var key : char;
   line : TStringList;
   l : string;

begin (* BLOCO PRINCIPAL *)
   TRY
   {$IFDEF WIN32}SetConsoleTitle('Periodic Table 1.0.0 (EN) - Igor Nunes Software, 2013');{$ENDIF}
   Verify;
   
   table := TStringList.Create;
   table.LoadFromFile(ATOMS_DB);
   table.Delete(0);
   
   Table_Only_Group := TStringList.Create;
   Table_Only_Period := TStringList.Create;
   Table_Only_XY := TStringList.Create;
   Table_Only_Symbol := TStringList.Create;
   
   line := TStringList.Create;
   for l in table do begin
      line.Clear;
      Split(l, '|', line);
      table_Only_XY.Append(line[IND_GROUP] + '|' + line[IND_PERIOD]);
      table_Only_Group.Append(line[IND_GROUP]);
      table_Only_Period.Append(line[IND_PERIOD]);
      Table_Only_Symbol.Append(line[IND_SYMBOL]);
   end;
   line.Destroy;
   
   MenuTable(5, 23, key);
   
   table.Destroy;
   Table_Only_Group.Destroy;
   Table_Only_Period.Destroy;
   Table_Only_XY.Destroy;
   Table_Only_Symbol.Destroy;
   
   EXCEPT
      on ex : Exception do RaiseErrorEvent(ex);
   END;
end.