(* * * * * * * * * * * * * === Unit  Auxiliar === * * * * * * * * * * * *
 *      Version: 1.0.0                                                  *
 * Release date: December 31st, 2012                                    *
 *      License: GNU GPL 3.0  (included with the source)                *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt  *
 *                                                                      *
 *  Description: Miscellaneous tools.                                   *
 * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * *)

unit UAuxiliar;

interface
uses crt, classes;

TYPE TKeys = set of char;

procedure Split(const st : string; const sep : char; var ast : TStringList);
function str2int(const s : string) : integer;
function int2str(const i : integer) : string;
procedure Pause(const PauseText : string; const KeysToProceed : TKeys; var KeyReceiver : char);
procedure Pause(const PauseText : string; const KeysToProceed : TKeys); overload;
procedure Pause(const PauseText : string); overload;
procedure Pause; overload;


implementation

procedure Split(const st : string; const sep : char; var ast : TStringList);
(* Divide 'st' pelo caracter 'sep' e guarda as partes em 'ast'. *)
var j : integer;
    t : string;
begin
   t := '';
   for j:=1 to length(st) do begin
      if ((st[j] = sep) and not(t = '')) or (j = length(st)) then begin
         if (j = length(st)) and not(st[j] = sep) then t := t + st[j];
         ast.Append(t);
         t := '';
      end else
         t := t + st[j];
   end;
end;


function str2int(const s : string) : integer;
(* Devolve uma String convertida num Integer *)
begin Val(s, str2int); end;

function int2str(const i : integer) : string;
begin Str(i, int2str) end;


procedure Pause(const PauseText : string; const KeysToProceed : TKeys; var KeyReceiver : char);
(* PROCEDIMENTO COMPLETO - recebe texto a mostrar, teclas que desbloqueiam a pausa e a vari�vel receptora da chave *)
begin
     write(PauseText);
     repeat
           KeyReceiver := ReadKey;
     until KeyReceiver in KeysToProceed;
end;


procedure Pause(const PauseText : string; const KeysToProceed : TKeys); overload;
(* PROCEDIMENTO COMPACTO - n�o h� output *)
var Key : char;
begin
     Pause(PauseText, KeysToProceed, Key);
end;


procedure Pause(const PauseText : string); overload;
(* PROCEDIMENTO SIMPLIFICADO - assume Enter como tecla a processar *)
begin
     Pause(PauseText, [#13]);
end;


procedure Pause; overload;
const StrEmpty : string = '';
begin
     Pause(StrEmpty);
end;

end.