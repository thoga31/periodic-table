Periodic Table

1.0.0

Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt

Licensed under the GNU GPL 3.0 (https://www.gnu.org/copyleft/gpl.html)

Language: Object Pascal (using Free Pascal Compiler, only)

This application provides a basic interactive periodic table.
Its information is stored in a text-based database, 'DB.periodic', provided in the folder 'db'.